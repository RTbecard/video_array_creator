# vids2array

A python script for stitching and synchronizing multiple videos into a single video file.

![Screenshot](screenshot.jpg)



##  Installation

You can use pip to install `vids2array` directly from this gitlab page.  On `bash` (UNIX):

```bash
$pip3 install https://gitlab.com/RTbecard/video_array_creator/-/archive/master/video_array_creator-master.tar.gz
```

On `powershell` (windows):

```powershell
>pip3.exe install https://gitlab.com/RTbecard/video_array_creator/-/archive/master/video_array_creator-master.tar.gz
```

`vids2array` effectively just writes a long command for the program `ffmpeg`, hence you need to make sure you have that installed on your system and that it's visible in your `PATH` variable for your shell.  

On windows, you can download the latest release [here](https://www.gyan.dev/ffmpeg/builds/ffmpeg-release-essentials.zip).  Just extract the contents to somewhere sensible like `C:\Program Files\ffmpeg`, and thats it.

## Usage:

`vids2array` is designed to be called directly from the command line.  `bash` or `zsh` for UNIX systems, or `powershell` for windows, just run `python3 -m vids2array` .  To get an idea how it works, just launch the help documentation.

	$python3 -m vids2array -h

Here's a walk through of how to use `vids2array` on windows.  First, we'll add the install location of `ffmpeg` to our `PATH` variable.  This should be the folder where `ffmpeg` and `ffprobe` is installed.  We'll verify this works be checking the version of number of `ffmpeg`.

```powershell
> $Env:PATH += ";C:\Program Files\ffmpeg\ffmpeg-5.1-essentials_build\bin"
> echo $Env:PATH
> ffmpeg -version
```

Now, suppose we have a folder in our `Documents` folder with the following structure:

```
videos
├── D09_20220503132241BirdViewRiver_Site1_.mp4
├── D11_20220503132241BirdViewRiver_Site2_.mp4
├── D13_20220503101756BirdViewRiver_Site3_.mp4
├── D14_20220503102356BirdViewRiver_Site1_.mp4
├── D15_20220503102256BirdViewRiver_Site2_.mp4
└── meta.csv
```

The following command would be used to extract a 1 minute segment from all the cameras, starting from the time `10:20`, and arrange them into a video array:

```powershell
> python.exe -m vids2array -i ~/Documents/videos -o ~/Documents/videos/output 10:20:00 60 ~/Documents/videos/meta.csv "_" 3 "mp4"
```

The last two arguments `"_"` and `3` are used for extracting the camera ID from the video file names.  File names will be split into fields, using `"_"` as a delimiter.  Then, the value of the third field, in this case, would be used.  Hence, our camera IDs are `Site1`, `Site2`, and`Site3`.  Videos for each of these camera IDs will be concatenated into a sequence, based on alphabetical order, and each camera will have a panel in the resulting output video.

The `meta.csv` holds the information used to synchronize the timestamps on all the videos.  See the help documentation for details and an example on how to correctly format this.
