from setuptools import setup, find_packages

setup(
    name="vids2array",
    version="0.3",
    packages=["vids2array"],
    package_dir={"vids2array": "src/vids2array"},
    include_package_data=True,
    package_data={'vids2array': ["help.txt"]},
    author="James Campbell & Jeroen Hubert",
    author_email="jamesadamcampbell@gmail.com",
    install_requires=[
          'pandas', 'numpy']
)
