#===============================================================================
# Copyright 2019, James Campbell & Jeroen Hubert
#
# This file is part of vids2array.
#
# vids2array is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# vids2array is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with vids2array.  If not, see <https://www.gnu.org/licenses/>.
#===============================================================================

import pandas as pd
import sys
import argparse
import os
import subprocess
import glob
import pkg_resources
import numpy as np

from vids2array.ffmpeg import input_videos, video_meta, background, complex_filter
from vids2array.input import get_camera_videos, parse_videos

## Launch screen
splash = """
------ Video Array Generator ------
Authors:	Jeroen Hubert & James Campbell
Version:	0.3
Last update: 	2022-08-02
License:	GPL-3
"""

print(splash)

help_path = pkg_resources.resource_filename('vids2array', '/help.txt')

# Documentation string
with open(help_path) as f:
    help_txt = "".join(f.readlines())

# Make argument aprser
parser = argparse.ArgumentParser(
	description=help_txt,
	add_help=True,
	formatter_class=argparse.RawTextHelpFormatter)

parser.add_argument("start_time", type=str,
					help="Start time in format 'hh:mm:ss' to begin video array.")
parser.add_argument("duration", type=float,
					help="Duration in seconds of the resulting video array.")
parser.add_argument("meta", type=str,
					help="Path to camera metadata csv file.  Each row indicated a camera.  Columns are 'camera id', 'global start time', and 'local start time'.")
parser.add_argument("delim", type=str,
					help="Character delimiter for parsing video file names.")
parser.add_argument("field", type=int,
					help="Field containing the camera id, parsed from the video file names.")
parser.add_argument("ext", type=str,
					help="Extension for video files, such as 'mp4'.")

parser.add_argument("-i", type=str, default=os.getcwd(),
					help="Path to folder holding video files.")
parser.add_argument("-o", type=str, default=os.path.join(os.getcwd(), "output"),
					help="Path to output folder.  By default, this is a folder named 'output' in the current working directory.")

# Parse input options
args = parser.parse_args()

path = os.path.expanduser(args.i)
path_out = os.path.expanduser(args.o)
meta = args.meta
delim = args.delim
field = args.field
ext = args.ext

# Function: Convert time stamps into seconds
def time2sec(str):
	parse = str.split(":")
	return int(parse[0])*60*60 + int(parse[1])*60 + int(parse[2])

# Validate timestamp and duration arguments
try:
	extract_start = time2sec(args.start_time)
except:
	sys.exit("Invalid format for <start time>")
try:
	extract_duration = int(args.duration)
except:
	sys.exit("Invalid format for <duration>")

# ------ Cehck output directory
if not os.path.exists(path_out):
  # Create a new directory because it does not exist
  os.makedirs(path_out)

# ------ Print script parameters
print("\n====== User Parameters ======")
print("--- Paths")
print("Videos Folder: ", path)
print("Output Folder: ", path_out)
print("Metadata file: ", meta)
print("--- File parsing")
print("Delimiter: ", delim)
try:
	field = int(field)
	print("Field: ", field)
except:
	sys.exit("You must provide in integer value to -f")
print("--- Extraction params")
print("Start time: ", extract_start)
print("Duration: ", extract_duration)

# ============================ Parse metadata =================================
print("\n====== Metadata ======")
meta_df = pd.read_csv(meta, header = None,
	names = ["Camera", "sync_time_global", "sync_time_local"])

# Convert times to seconds
print("converting times to seconds in metadata csv...")
try:
	meta_df['sync_time_global'] = meta_df['sync_time_global'].apply(time2sec)
except:
	print("Error: Check formatting of global times")
try:
	meta_df['sync_time_local'] = meta_df['sync_time_local'].apply(time2sec)
except:
	print("Error: Check formatting of global times")

# Add real start time to meta
meta_df["start_time_global"] = meta_df["sync_time_global"] - \
	meta_df["sync_time_local"]

print(meta_df)

# =========================== Parse video names ===============================
# ------ Parse video names
videos_df, videos = parse_videos(path, ext, delim, field)

print("\n==== Video list preview ======")
print("Video search path: ", path)
print("------ Available video files")
print(videos)
print("------ Used videos")
print(videos_df)

# ======================== Align videos with cameras ==========================
# ------ Make list of videos and start/end times for each camera
vids_per_cam = get_camera_videos(
	path, extract_start, extract_duration, videos_df, meta_df)

print("------ List of video segments for each camera")
print(vids_per_cam)

if len(vids_per_cam) == 0:
	raise ValueError("No videos available for input parameters.")

# ------ Get metadata from first video file
width, height, fps = video_meta(path, vids_per_cam["file"].values[0])

# ------ Define video matrix size (rows and columns)
cameras = pd.unique(vids_per_cam["panel"])
cameras_n = len(cameras)
panels_cols = int(np.ceil(np.log2(cameras_n)))
panels_df = pd.DataFrame({"Panel": cameras})
panels_df["row"] = np.ceil(cameras/panels_cols).astype(int)
panels_df["col"] = np.ceil(np.mod(cameras - 1, panels_cols) + 1).astype(int)
panels_rows = max(panels_df["row"])

print("---- Camera array structure")
print(panels_df)

#===============================================================================
#====================== Run Command ============================================
## Make output folder if does not exist
if not os.path.isdir(path_out):
	os.mkdir(path_out)

# Initialize command list
command_list = ["ffmpeg", "-y", "-loglevel", "warning", "-hide_banner"]

# Add input parameters
command_list += input_videos(path, vids_per_cam)
# Add black background (as input stream)
command_list += background(
	extract_duration, width*panels_cols, height*panels_rows, fps)

# Add complex filter (this concatenates and voerlays the videos into a array)
command_list.append("-filter_complex")
command_list.append(
	complex_filter(vids_per_cam, videos_df, panels_df, width, height))


# time = "[panels] drawtext=text='" + \
# 	timestamp + \
# 		"':x=(w-text_w):y=(h-text_h):fontfile=UbuntuMono-R.ttf:fontsize=" + \
# 			str(textsize) + \
# 				":fontcolor=white"

## Add output arguments
command_list.append( "-t")
command_list.append( str(extract_duration))
# command_list.append("-c:v")
# command_list.append("libx264")
command_list.append(
	os.path.join(path_out, "start-%i_duration-%i.mp4" %
			 (extract_start, extract_duration)))

print("\n====== ffmpeg command ======")
print(" ".join(command_list))
print("\n")

# save command to file
fname = os.path.join(path_out, "ffmpeg_command.txt")
f = open(fname, mode = "w")
f.write("\n".join(command_list))
f.close()

print("... runnning ffmpeg, please wait...")
result = subprocess.run(command_list, capture_output= True)
print(str(result.stderr).replace("\\n","\n").replace("\\r",""))

