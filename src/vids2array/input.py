import pandas as pd
import sys
import argparse
import os
import subprocess
import glob

def parse_videos(path, ext, delim, field):
	"""
	Parse video file names and match videos to camera IDs.
	"""

	###### Load video files and extract camera ID
	videos = glob.glob("**" + ext, recursive=True, root_dir=path)

	def cameraID(string):
		return os.path.basename(string).split(delim)[field - 1]
	videos_df = pd.DataFrame({"File": videos,
		"CameraID":list(map(cameraID, videos))})

	return(videos_df, videos)

def get_camera_videos(path, extract_start, extract_duration, videos_df, meta_df):
	"""
	Make a list of ordered video files to extract footage from.

	Parameters:
		- meta_df: Dataframe holding the syncronization data for each camera.
		- videos_df: dataframe holding the paired file names and camera IDs.

	Return:
		Dataframe will hold the filename, start time, and end time, for each
		video file to be appended for that camera.
		The stream index and panel numbers will also be returned.
		Resulting columns will be: file, start, end, stream, panel.

	Naming conventions
	 - global: real world time.
	 - local: time relative to first file in camera.
	 - EOS: end of video stream for this camera.
	"""

	print("\n==== Video segments to extract ======")
	idx_panel = 1 # track position of vodei in output matrixz
	vids_per_cam = pd.DataFrame() # Init empty dataframe for timestamps

	# ---- Loop Cameras
	for i in list(range(0,meta_df.iloc[:,1].size ,1)):

		# Extract files with matching camera ID
		idx = videos_df['CameraID'] == meta_df.iloc[i,0]
		subset = list(videos_df.loc[idx,'File'])

		# Init dataframe with start and end times
		start_file_local = 0
		start_camera_global = meta_df.iloc[i,3]

		# ------ Loop through videos from matching camera
		for j in list(range(0,len(subset))):

			# ---- ffpmeg to find video duration
			result = subprocess.run(
				["ffprobe", "-show_entries","format=duration",
					os.path.join(path,subset[j])], capture_output=True)
			result_parse = str(result.stdout).split("\\n")

			# ---- Parse stdout
			result_idx = [k for k, val in enumerate(result_parse) \
				if "duration" in val]
			if(len(result_idx) != 1):
				sys.exit("Error reading video duration: " + subset[j])
			duration_file = float(result_parse[result_idx[0]].split("=")[1].replace("\\r",""))
			end_file_local = start_file_local + duration_file
			end_file_global = end_file_local + start_camera_global

			# ---- Mark video times to extract (also if any should be extracted)
			temp = pd.DataFrame({"file":[subset[j]],
			 "start":[0],
			 "end": [duration_file]})
			export = False # When True, video will be marked for ffmpeg
			EOS = False # End of segment to extract reached
			start_file_global = start_file_local + start_camera_global

			# ------ Is start time inside video
			if (extract_start > start_file_global) & (extract_start < end_file_global):
				temp["start"] = extract_start - start_file_global
				export = True

			# ------ Is end time inside video
			if ((extract_start + extract_duration) < end_file_global) & \
				((extract_start + extract_duration) > start_file_global):
				temp["end"] = (extract_start + extract_duration) - start_file_global
				export = True
				EOS = True

			# ---- Add input/panel indicies to dataframe
			temp["panel"] = idx_panel
			# Append time stamps to data.frame
			if export:
				vids_per_cam = pd.concat([vids_per_cam, temp])
				if EOS:
					# no more video streams to read.  end loop for camera
					break
			# increment local cameta start time
			start_file_local += duration_file

		# Increment panel number
		idx_panel += 1

	return(vids_per_cam)
