import pandas as pd
import sys
import argparse
import os
import subprocess
import glob
import numpy as np

def input_videos(path, vids_per_cam):

	# ------ Write input parameter for each video file
	input_videos = list()
	for i in range(0, len(vids_per_cam)):
		input_videos.append("-ss")
		input_videos.append("%f" % (vids_per_cam["start"].values[i]))

		input_videos.append("-t")
		input_videos.append("%f" % (vids_per_cam["end"].values[i]))

		input_videos.append("-i")
		input_videos.append(os.path.join(path,vids_per_cam["file"].values[i]))

	return(input_videos)

def background(duration_file, width, height, fps):
	"""
	Make a black rectangle as a video stream.  For use as a background overlay.
	"""
	background = list()
	background.append("-f")
	background.append("lavfi")
	background.append("-i")
	background.append("color=c=black:duration=%f:size=%ix%i:rate=%f" % \
		(duration_file, width, height, fps))

	return(background)

def video_meta(path, file):
	"""
	Extract metadata from video file
	"""

	path_abs = os.path.join(path,file)
	# ------ Get dimensions of videos
	args = ["ffprobe", "-show_entries", "stream=width,height,r_frame_rate",
		 path_abs]
	result = subprocess.run(args, capture_output=True)
	result_parse = str(result.stdout).replace("\\r","").split("\\n")

	# Parse stdout
	vid_width = int(result_parse[1].split("=")[1])
	vid_height = int(result_parse[2].split("=")[1])
	fps = float(result_parse[3].split("=")[1].split("/")[0])

	return(vid_width, vid_height, fps)

def complex_filter(vids_per_cam, videos_df, panels_df, vid_width, vid_height):
	"""
	Build ffmpeg command
	- Each marked file needs to be added as an ordered input to ffmpeg
	- Cameras with multiple videos need to have these videos concatenated
	- The video output size will be determined by the number of unique cameras
		and the resulting matrix size that will fit these videos
	"""


	# init string for filter complex
	filter_complex = ""

	# ------ Create panel objects (concatenated videos per camera)
	streams = 0
	for i in range(0,len(panels_df)):
		# Get indicies for streams which match this panel
		idx_sub = vids_per_cam.loc[vids_per_cam["panel"] == (i + 1),].index
		# Write stream order
		for j in idx_sub:
			filter_complex += "[%i:v]" % (streams)
			streams += 1

		# Filter specification
		filter_complex += " concat=n=%i:v=1:a=0 [p%i]; " % (len(idx_sub), i)

	# ------ Arrange panel objects in matrix
	def vstrm(x): return "[str" + str(x) + "]"

	# ------ Create black background to overlap panels on
	background_stream = "[" + str(np.max(vids_per_cam["panel"])) + ":v]"

	# ------ Create panel overlay
	for i in range(0, len(panels_df)):
		# get positon of panel
		panel_x = vid_width*(panels_df["col"].values[i] - 1)
		panel_y = vid_height*(panels_df["row"].values[i] - 1)

		## Write overlay params
		panel_overlay = "[p%i] overlay=x=%i:y=%i" % (i, panel_x, panel_y)

		if(i == 0):
			# Overlay first panbel on background stream
			filter_complex += background_stream + panel_overlay
		else:
			# Overlay panel on existing panel overlay
			filter_complex += "[str%i]" % (i - 1)
			filter_complex += panel_overlay

		if i != len(panels_df) - 1:
			filter_complex += " [str%i]; " % (i)

	return(filter_complex)
